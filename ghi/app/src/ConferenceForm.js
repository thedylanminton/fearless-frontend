import React, { useState, useEffect } from 'react';

function ConferenceForm({ getConferences }) {
  const [name, setName] = useState('');
  const [starts, setStarts] = useState('');
  const [ends, setEnds] = useState('');
  const [description, setDescription] = useState('');
  const [maxPresentations, setMaxPresentations] = useState('');
  const [maxAttendees, setMaxAttendees] = useState('');
  const [location, setLocation] = useState('');
  const [locations, setLocations] = useState([]);

  async function fetchLocations() {
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  useEffect(() => {
    fetchLocations();
  }, [])

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      name,
      starts,
      ends,
      description,
      location,
      max_presentations: maxPresentations,
      max_attendees: maxAttendees,
    };

    const locationUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);
      setName('');
      setStarts('');
      setEnds('');
      setDescription('');
      setMaxPresentations('');
      setMaxAttendees('');
      setLocation('');
      getConferences()
    }
  }

  function handleChangeName(event) {
    const { value } = event.target;
    setName(value);
  }

  function handleChangeStarts(event) {
    const { value } = event.target;
    setStarts(value);
  }

  function handleChangeEnds(event) {
    const { value } = event.target;
    setEnds(value);
  }

  function handleChangeDescription(event) {
    const { value } = event.target;
    setDescription(value);
  }

  function handleChangeMaxPresentations(event) {
    const { value } = event.target;
    setMaxPresentations(value);
  }

  function handleChangeMaxAttendees(event) {
    const { value } = event.target;
    setMaxAttendees(value);
  }

  function handleChangeLocation(event) {
    const { value } = event.target;
    setLocation(value);
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input value={name} onChange={handleChangeName} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={starts} onChange={handleChangeStarts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" />
              <label htmlFor="starts">Starts</label>
            </div>
            <div className="form-floating mb-3">
              <input value={ends} onChange={handleChangeEnds} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
              <label htmlFor="ends">Ends</label>
            </div>
            <div className="mb-3">
              <label htmlFor="description">Description</label>
              <textarea value={description} onChange={handleChangeDescription} className="form-control" id="description" rows="3" name="description"></textarea>
            </div>
            <div className="form-floating mb-3">
              <input value={maxPresentations} onChange={handleChangeMaxPresentations} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
              <label htmlFor="max_presentations">Maximum presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input value={maxAttendees} onChange={handleChangeMaxAttendees} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
              <label htmlFor="max_attendees">Maximum attendees</label>
            </div>
            <div className="mb-3">
              <select value={location} onChange={handleChangeLocation} required name="location" id="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.id}>{location.name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ConferenceForm;
